<?php
namespace Jokuf\Meta\Social;


abstract class OpenGraph
{
    public function __get($prop){
        if(array_key_exists($prop, $this->og)){
            return $this->og[$prop];
        }

        return null;
    }   

    public function __set($prop, $value){
        if(!isset($this->og[$prop])){
            throw new \Exception("Trying to add unknown tag");
        }
        $this->og[$prop] = $value;
    }

    public function __isset($prop){
        return isset($this->og[$prop]);
    }

    public function __unset($prop){
        return unset($this->og[$prop]);
    }

    public function dump(){
        return $this->og;
    }
}