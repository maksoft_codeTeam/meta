<?php
namespace Jokuf\Meta\Social;


class Facebook extends OpenGraph
{
    $og = array(
        "url"         => null,
        "type"        => "article",
        "title"       => null,
        "description" => null,
        "image"       => null,
        "app_id"      => null,
    );

    public function __construct($title, $description, $image, $url=null, $app_id=null){
        $this->og["title"] = $title;

        $this->og["description"] = $description;

        $this->og["image"] = $image;

        $this->og["url"] = $url;

        $this->og["app_id"] = $app_id;
    }

    public function __toString(){
        $tags = array();
        foreach ($this->og as $propery => $content) {
            if(!$content){
                continue;
            }
            $tags[] = sprintf("<meta property=\"og:%s\" content=\"%s\" />", $propery, $content);
        }

        return implode(PHP_EOL, $tags);
    }
}