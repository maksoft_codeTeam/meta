<?php
namespace Jokuf\Meta\Social;


class Twitter extends OpenGraph{
    $og = array(
    	"card".       => "summary_large_image",
        "description" => null,
        "title"       => null,
        "image"       => null,
        "image:alt"   => null,
    );

    public function __construct($title, $description, $image, $image_alt=null){
        $this->og["title"] = $title;

        $this->og["description"] = $description;

        $this->og["image"] = $image;

        $this->og["image:alt"] = $image_alt;
    }
    public function __toString(){
        $tags = array();
        foreach ($this->og as $og_type => $content) {
            if(!$content){
                continue;
            }
            $tags[] = sprintf("<meta name=\"twitter:%s\" content=\"%s\" />", $og_type, $content);
        }

        return implode(PHP_EOL, $tags);
    }
}