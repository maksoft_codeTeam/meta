<?php
namespace Jokuf\Meta;


class Meta
{
	protected $metadata_default = array(
        'title'               => false,
        'description'         => false,
        'og_description'      => false,
        'twitter_description' => false,
        'gplus_description'   => false,
        'keywords'            => false,
        'image'               => false,
        'object_type'         => false,
        'og_type'             => false,
        'og_app_id'           => false,
        'og_profile_id'       => false,
        'og_publisher'        => false,
        'og_author_url'       => false,
        'fb_pages'            => false,
        'twitter_type'        => false,
        'twitter_site'        => false,
        'twitter_author'      => false,
        'gplus_type'          => false,
        'gplus_author'        => false,
        'gplus_publisher'     => false,
        'published_time'      => false,
        'modified_time'       => false,
        'expiration_time'     => false,
        'tag'                 => false,
        'url'                 => false,
        'locale'              => false,
        'custom_namespace'    => false,
	);

	const META_DESCRIPTION_LENGTH = 155;
	const META_TITLE_LENGTH = 70;

	public function set_keywords(Keyword $keyword)
	{
		$this->metadata_default['keywords'] = $keyword;
	}

	public function set_description(Description $description)
	{
		$this->metadata_default['description'] = $description;
	}

	public function set_title(Title $title)
	{
		$this->metadata_default['title'] = $title;
	}
}