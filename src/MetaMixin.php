<?php
namespace Jokuf\Meta;


/**
* 
*/
class MetaMixin
{

    public static function convert_text_to_keywords($content){
        $tmp_keywords_arr = explode(" ", strip_tags($content));
        $keywords_arr = array();

        foreach($tmp_keywords_arr as $keyword){
            
            $keyword = trim($keyword);

            if(strlen($keyword) < 8){
                continue;
            }

            if(isset($keywords_arr[$keyword])){
                $keywords_arr[$keyword] += 1;
                continue;
            }

            $keywords_arr[$keyword] = 1;
        }

        return array("keywords_arr" => $keywords_arr, "total" => count($tmp_keywords_arr));
    }

    public static function calc_keywords_density($content){
        $keywords = self::convert_text_to_keywords($content);
        $computed_data = array();
        $total_words = $keywords['total'];
        foreach($keywords['keywords_arr'] as $keyword => $occurances){
            /*
             *   The formula to calculate your keyword density on a web page for SEO purposes is 
             *   (Nkr/Tkn)*100, where Nkr is how many times you repeated a specific keyword and Tkn the total *
             *   words in the analyzed text. This will result in a keyword density value. When calculating keyword *   density, be sure to ignore html tags and other embedded tags which will not actually appear in 
             *   the text of the page once it is published.
             */
            $density = ( $occurances / $total_words ) * 100;
            $computed_data[] = array("name" => $keyword, "density" => $density, "occurances" => $occurances);
        }

        return self::sort_keywords($computed_data);
    }

    public static function sort_keywords($keywords_arr){
        $tmp = array();

        foreach($keywords_arr as &$keyword){
            $tmp[] = $keyword['density'];
        }
        array_multisort($tmp, $keywords_arr);

        return $keywords_arr;
    }
}